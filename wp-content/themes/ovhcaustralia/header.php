<!DOCTYPE html>
<html lang="en" class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
<?php wp_head(); ?>
</head>
<body>
	<?php 
		$defaults = array(
			'theme_location'  =>'primary' ,
			'menu'            => '',
			'container'       => '',					
			'container_id'    => '',
			'menu_class'      => '',
			'menu_id'         => '',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'items_wrap'      => '%3$s',//
			'depth'           => 0,
			'walker'          =>''
		);
	?>

	<div class="navbar navbar-inverse navbar-fixed-top pathways-header-fluid">
	    <div class="container header pathways-header-container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#PrimaryNavbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand header-navbar-brand" href="#">Overseas Visitors Health Cover Australia</a>
			</div>
			<div class="collapse navbar-collapse" id="PrimaryNavbar">
				<ul class="nav navbar-nav navbar-right" id="menu-primary-navigation">
					<?php wp_nav_menu( $defaults ); ?>
				</ul>
			</div>
	    </div>
  	</div>
