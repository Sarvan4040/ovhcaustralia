<?php
/*
Template Name: Result
*/ 
include("includes/configure.php");

$title = get_the_title();

$FundsTypeArr = array("Single"=>"single only (1 adult only)","Couple"=>"couple only","Family"=>"family with children","Single Parent"=>"1 adult & children");

if(!empty($title)) {

  $fund_selected = $title;
  $selected_fund = $FundsTypeArr[$title];

  $find=$dbconn->prepare("SELECT * FROM  tbl_ovhc_policies WHERE policy_type=:fund_selected order by weekly_premimum_price asc");
  $find->execute(array(':fund_selected'=>$fund_selected));
  $resultArr=$find->fetchAll(PDO::FETCH_ASSOC);

  $fundDetailsArr = array_merge_recursive($resultArr[0], $resultArr[1],$resultArr[2],$resultArr[3], $resultArr[4],$resultArr[5],$resultArr[6]);
} else {
  $fund_selected = "Single";
  $selected_fund = "single only (1 adult only)";

  $find=$dbconn->prepare("SELECT * FROM  tbl_ovhc_policies WHERE policy_type=:fund_selected order by weekly_premimum_price asc");
  $find->execute(array(':fund_selected'=>$fund_selected));
  $resultArr=$find->fetchAll(PDO::FETCH_ASSOC);

  $fundDetailsArr = array_merge_recursive($resultArr[0], $resultArr[1],$resultArr[2],$resultArr[3], $resultArr[4],$resultArr[5],$resultArr[6]);
}

get_header();
?>

  <div class="result-banner-fluid">
    <div class="container result-contain">
      <div class="result-inside-container">
        <h1 class="result-top-text">Working Visa Health Cover</h1>
        <h3 class="result-sub-text">For <?php echo $selected_fund; ?></h3>
        <p></p>
      </div>
    </div>
  </div>

  <div class="container result-container">
    <div class="text-center spinner-container"></div>
    <form name="result_form" id="result_form" action="" method="post">
      <input type="hidden" id="displayed_column" name="displayed_column" value='1,2,3,4,5'>
      <input type="hidden" id="funds_type" name="funds_type" value='<?php echo $fund_selected; ?>'>
      <p>&nbsp;</p>
      <!-- <h3 class="result-title">Below quote is for <?php echo $selected_fund; ?></h3> -->
      <p class="other-types">For other quotes, please select here:&nbsp;&nbsp;&nbsp;
        <select name="cover_type" id="cover_type">
          <?php 
          foreach ($FundsTypeArr as $FundsTypeArrkey => $FundsTypeArrvalue) {
            if ($fund_selected == $FundsTypeArrkey)
              echo '<option value="'.site_url().'/'.$FundsTypeArrkey.'" selected>'.$FundsTypeArrvalue.'</option>';
            else
              echo '<option value="'.site_url().'/'.$FundsTypeArrkey.'">'.$FundsTypeArrvalue.'</option>';
          } ?>
        </select>
      </p>
      
      <div class="col-lg-12 col-md-12 col-md-12 hidden-xs">
        <div class="col-lg-12 col-md-12">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <p>&nbsp;</p>
            <table class="nav-table">
              <tr>
                <td>&nbsp;</td>
                <td class="previous-item"><button type="button" name="previous-btn" class="previous-btn nav-btn"><img class="img-responsive prev" src="<?php echo get_stylesheet_directory_uri() ?>/images/prev.png">&nbsp;&nbsp;&nbsp;Prev</button></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td class="next-item"><button type="button" name="next-btn" class="next-btn nav-btn">Next&nbsp;&nbsp;&nbsp;<img class="img-responsive next" src="<?php echo get_stylesheet_directory_uri() ?>/images/next.png"></button></td>
              </tr>
            </table>
            <table class="result-table">
              <tr>
                <td>&nbsp;</td>
                <?php $i=1;
                foreach ($fundDetailsArr['policy_image_url'] as $policy_image_url_key => $policy_image_url) {
                  if ($i==6 || $i==7) {
                    echo '<td class="results-column hide '.$i.'"><img class="img-responsive" src="'.$policy_image_url.'"></td>';
                  } else{
                    echo '<td class="results-column '.$i.'"><img class="img-responsive" src="'.$policy_image_url.'"></td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <?php $i=1;
                foreach ($fundDetailsArr['policy_company'] as $policy_company_key => $policy_company) {
                  if ($i==6 || $i==7) {
                    // echo '<td class="results-column row-dark-blue fund-name hide '.$i.'"><p class="policy-company">'.$policy_company.'</p><span class="policy-company">'.$fundDetailsArr['policy_name'][$policy_company_key].'</span></td>';
                    echo '<td class="results-column row-dark-blue fund-name hide '.$i.'"><span class="policy-company">'.$fundDetailsArr['policy_name'][$policy_company_key].'</span></td>';
                  } else{
                    // echo '<td class="results-column row-dark-blue fund-name '.$i.'"><p class="policy-company">'.$policy_company.'</p><span class="policy-company">'.$fundDetailsArr['policy_name'][$policy_company_key].'</span></td>';
                    echo '<td class="results-column row-dark-blue fund-name '.$i.'"><span class="policy-company">'.$fundDetailsArr['policy_name'][$policy_company_key].'</span></td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <?php $i=1;
                foreach ($fundDetailsArr['weekly_premimum_price'] as $weekly_premimum_price_key => $weekly_premimum_price) {
                  if ($i==6 || $i==7) {
                    echo '<td class="row-light-gray results-column hide '.$i.'"><p class="premium">$'.$weekly_premimum_price.'</p><p class="premium-type">Weekly</p><a href="javascript:void(0);" data-url="'.$fundDetailsArr['policy_url'][$weekly_premimum_price_key].'" name="buy_now" class="buy-now-btn">Select Now</a><br><br></td>';
                  } else{
                    echo '<td class="row-light-gray results-column '.$i.'"><p class="premium">$'.$weekly_premimum_price.'</p><p class="premium-type">Weekly</p><a href="javascript:void(0);" data-url="'.$fundDetailsArr['policy_url'][$weekly_premimum_price_key].'" name="buy_now" class="buy-now-btn">Select Now</a><br><br></td>';
                  }
                  $i++;
                } ?>
              </tr>
              <!-- <tr>
                <td class="row-dark-blue row-header">General</td>
                <td colspan="5" class="row-header">&nbsp;</td>
              </tr>
              <tr>
                <td class="row-gray">Level of cover</td>
                <?php $i=1;
                foreach ($fundDetailsArr['level_of_cover'] as $level_of_cover_key => $level_of_cover) {
                  if ($i==6 || $i==7) {
                    echo '<td class="row-gray results-column hide '.$i.'">'.$level_of_cover.'</td>';
                  } else{
                    echo '<td class="row-gray results-column '.$i.'">'.$level_of_cover.'</td>';
                  }
                  $i++;
                } ?>
              </tr> -->
              <tr>
                <td class="row-light-blue">About this cover</td>
                <?php $i=1;
                foreach ($fundDetailsArr['about_this_cover'] as $about_this_cover_key => $about_this_cover) {
                  if ($i==6 || $i==7) {
                    echo '<td class="row-light-blue results-column hide '.$i.'">'.$about_this_cover.'</td>';
                  } else{
                    echo '<td class="row-light-blue results-column '.$i.'">'.$about_this_cover.'</td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-dark-blue row-header">Visa Compliance</td>
                <td colspan="5" class="row-header">&nbsp;</td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Does this health insurance cover meeds the condition of the Australian Department of Immigration and Border Protection's visa requirements.">Meets government requirements</td>
                <?php $i=1;
                foreach ($fundDetailsArr['meets_govt_requirements'] as $meets_govt_requirements_key => $meets_govt_requirements) {
                  if ($meets_govt_requirements=="Yes") 
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                  else
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                  if ($i==6 || $i==7) {
                    echo '<td class="row-gray results-column hide '.$i.'"><img src="'.$src.'" ></td>';
                  } else{
                    echo '<td class="row-gray results-column '.$i.'"><img src="'.$src.'" ></td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Some insurance will immediately issue a letter confirming your health insurance. You can use this letter as a proof of insurance with your visa application.">Instant visa letter issued</td>
                <?php $i=1;
                foreach ($fundDetailsArr['instant_visa_letter_issued'] as $instant_visa_key => $instant_visa) {
                  if ($instant_visa=="Yes") 
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                  else
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                  if ($i==6 || $i==7) {
                    echo '<td class="row-light-blue results-column hide '.$i.'"><img src="'.$src.'" ></td>';
                  } else{
                    echo '<td class="row-light-blue results-column '.$i.'"><img src="'.$src.'" ></td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-gray">Visa types</td>
                <?php $i=1;
                foreach ($fundDetailsArr['visa_types'] as $visa_types_key => $visa_types) {
                  if ($i==6 || $i==7) {
                    echo '<td class="row-gray results-column hide '.$i.'">'.$visa_types.'</td>';
                  } else{
                    echo '<td class="row-gray results-column '.$i.'">'.$visa_types.'</td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-dark-blue row-header">What's Included</td>
                <td colspan="5" class="row-header">&nbsp;</td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Helps towards the hospital accommodation costs and doctors, specialists, surgeons and anaesthetists fees for the hospital services (see some example services below).">Hospital expenses</td>
                <?php $i=1;
                foreach ($fundDetailsArr['hospital_expenses'] as $hospital_expenses_key => $hospital_expenses) {
                  if ($hospital_expenses=="Yes") 
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                  else
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                  if ($i==6 || $i==7) {
                    echo '<td class="row-light-blue results-column hide '.$i.'"><img src="'.$src.'" ></td>';
                  } else{
                    echo '<td class="row-light-blue results-column '.$i.'"><img src="'.$src.'" ></td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="When you’re treated in hospital by a doctor, surgeon or anaesthetist the set fee for their service is called the Medicare Benefits Schedule (MBS) fee. If the person treating you charges only the MBS fee you won’t need to pay anything. If they choose to charge above the MBS fee you will likely have to pay an out-of-pocket cost to cover the difference. ">Medical expenses (in hospital)</td>
                <?php $i=1;
                foreach ($fundDetailsArr['medical_expenses_inhospital'] as $medical_expenses_inhospital_key => $medical_expenses_inhospital) {
                  if ($medical_expenses_inhospital=="Yes") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "";
                  }
                  else if ($medical_expenses_inhospital=="Yes | 100% of MBS") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "100% of MBS";
                  }
                  else{
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                    $text = "";
                  }
                  if ($i==6 || $i==7) {
                    echo '<td class="row-gray results-column hide '.$i.'"><img src="'.$src.'" ><br>'.$text.'</td>';
                  } else{
                    echo '<td class="row-gray results-column '.$i.'"><img src="'.$src.'" ><br>'.$text.'</td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Admission into private hospital. Generally overnight accommodation in a private or shared room, same day admissions, intensive care, operating theatre fees, etc.">Private hospital admission</td>
                <?php $i=1;
                foreach ($fundDetailsArr['private_hospital_admission'] as $private_hospital_admission_key => $private_hospital_admission) {
                  if ($private_hospital_admission=="Yes") 
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                  else
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                  if ($i==6 || $i==7) {
                    echo '<td class="row-light-blue results-column hide '.$i.'"><img src="'.$src.'" ></td>';
                  } else{
                    echo '<td class="row-light-blue results-column '.$i.'"><img src="'.$src.'" ></td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Admission into public hospital as a private patient. Generally overnight accommodation in a private or shared room, same day admissions, emergency department facility fees (if the service leads to an admission or it is continuation of care following an admission)">Public hospital admission</td>
                <?php $i=1;
                foreach ($fundDetailsArr['public_hospital_admission'] as $public_hospital_admission_key => $public_hospital_admission) {
                  if ($public_hospital_admission=="Yes") 
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                  else
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                  if ($i==6 || $i==7) {
                    echo '<td class="row-gray results-column hide '.$i.'"><img src="'.$src.'" ></td>';
                  } else{
                    echo '<td class="row-gray results-column '.$i.'"><img src="'.$src.'" ></td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Any accidential injury. For accident to be covered, treatment must be sought through a Doctor or an Emergency Department within 48 hours of sustaining the injury.">Accidents & emergency services</td>
                <?php $i=1;
                foreach ($fundDetailsArr['accidents_and_emergency_services'] as $accidents_services_key => $accidents_services) {
                  if ($accidents_services=="Yes") 
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                  else
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                  if ($i==6 || $i==7) {
                    echo '<td class="row-light-blue results-column hide '.$i.'"><img src="'.$src.'" ></td>';
                  } else{
                    echo '<td class="row-light-blue results-column '.$i.'"><img src="'.$src.'" ></td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="When you needed an ambulance to be transported to hospital, health insurance providers will pay 100% cover for when you need an emergency ambulance services.">Emergency Ambulance</td>
                <?php $i=1;
                foreach ($fundDetailsArr['emergency_ambulance'] as $emergency_ambulance_key => $emergency_ambulance) {
                  if ($emergency_ambulance=="Yes") 
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                  else
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                  if ($i==6 || $i==7) {
                    echo '<td class="row-gray results-column hide '.$i.'"><img src="'.$src.'" ></td>';
                  } else{
                    echo '<td class="row-gray results-column '.$i.'"><img src="'.$src.'" ></td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="If someone on your membership has to return to your home country due to a substantial life-altering illness or injury, health providers may arrange and pay the reasonable cost of return travel with the appropriate medical supervision.">Repatriation to home country</td>
                <?php $i=1;
                foreach ($fundDetailsArr['repartation_to_home_country'] as $repartation_key => $repartation) {
                  if ($repartation=="Yes") 
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                  else
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                  if ($i==6 || $i==7) {
                    echo '<td class="row-light-blue results-column hide '.$i.'"><img src="'.$src.'" ></td>';
                  } else{
                    echo '<td class="row-light-blue results-column '.$i.'"><img src="'.$src.'" ></td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-dark-blue row-header">Excesses</td>
                <td colspan="5" class="row-header">&nbsp;</td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="This is the amount you pay towards your hospital treatment before health insurance providers make any benefits. It's paid once per person per calendar year. It does not apply to children on a family policy.">Excess option</td>
                <?php $i=1;
                foreach ($fundDetailsArr['excess_option'] as $excess_option_key => $excess_option) {
                  if ($i==6 || $i==7) {
                    echo '<td class="row-gray results-column hide '.$i.'">'.$excess_option.'</td>';
                  } else{
                    echo '<td class="row-gray results-column '.$i.'">'.$excess_option.'</td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-dark-blue row-header">Some examples of what services are covered</td>
                <td colspan="5" class="row-header">&nbsp;</td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="When you needed an ambulance to be transported to hospital, health insurance providers will pay 100% cover for when you need an emergency ambulance services.">Ambulance services</td>
                <?php $i=1;
                foreach ($fundDetailsArr['ambulance_services'] as $ambulance_services_key => $ambulance_services) {
                  if ($ambulance_services=="Yes") 
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                  else
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                  if ($i==6 || $i==7) {
                    echo '<td class="row-light-blue results-column hide '.$i.'"><img src="'.$src.'" ></td>';
                  } else{
                    echo '<td class="row-light-blue results-column '.$i.'"><img src="'.$src.'" ></td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Any accidential injury. For accident to be covered, treatment must be sought through a Doctor or an Emergency Department within 48 hours of sustaining the injury.">Accidential injury</td>
                <?php $i=1;
                foreach ($fundDetailsArr['accidential_injury'] as $accidential_injury_key => $accidential_injury) {
                  if ($accidential_injury=="Yes") 
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                  else
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                  if ($i==6 || $i==7) {
                    echo '<td class="row-gray results-column hide '.$i.'"><img src="'.$src.'" ></td>';
                  } else{
                    echo '<td class="row-gray results-column '.$i.'"><img src="'.$src.'" ></td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Services and treatment provided to an admitted patient in hospital that deal with the care of women during pregnancy, childbirth and following delivery. Includes regular and caesarean childbirth and in-patient diagnostic imaging, as well as treatment of pregnancy related complications.">Pregnancy and related services</td>
                <?php $i=1;
                foreach ($fundDetailsArr['pregnancy_and_related_services'] as $pregnancy_and_related_services_key => $pregnancy_and_related_services ) {
                  if ($pregnancy_and_related_services=="Yes") 
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                  else
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                  if ($i==6 || $i==7) {
                    echo '<td class="row-light-blue results-column hide '.$i.'"><img src="'.$src.'" ></td>';
                  } else{
                    echo '<td class="row-light-blue results-column '.$i.'"><img src="'.$src.'" ></td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Reconstructions to repair ligament tears, remove loose tissue and to treat other damage. Surgery to replace a hip or knee joint with a prosthesis. Its aim is to relieve pain and/or increase function by replacing all or part of the joint resurface.">Joint replacements surgery (hip, knee, shoulder)</td>
                <?php $i=1;
                foreach ($fundDetailsArr['join_replacements_surgery'] as $join_replacements_surgery_key => $join_replacements_surgery) {
                  if ($join_replacements_surgery=="Yes") 
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                  else
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                  if ($i==6 || $i==7) {
                    echo '<td class="row-gray results-column hide '.$i.'"><img src="'.$src.'" ></td>';
                  } else{
                    echo '<td class="row-gray results-column '.$i.'"><img src="'.$src.'" ></td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="A procedure to examine the lining of the large bowel (colon). It is used to investigate the cause of abdominal pain, bleeding, irregular bowel habits, to remove polyps or detect cancer.">Colonoscopy</td>
                <?php $i=1;
                foreach ($fundDetailsArr['colonoscopy'] as $colonoscopy_key => $colonoscopy ) {
                  if ($colonoscopy=="Yes") 
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                  else
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                  if ($i==6 || $i==7) {
                    echo '<td class="row-light-blue results-column hide '.$i.'"><img src="'.$src.'" ></td>';
                  } else{
                    echo '<td class="row-light-blue results-column '.$i.'"><img src="'.$src.'" ></td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="There are 200 different type of cancers and most area of the body can be affected. Some health insurance providers will cover some or all cancer treatments.">Cancer treatments</td>
                <?php $i=1;
                foreach ($fundDetailsArr['cancer_treatments'] as $cancer_treatments_key => $cancer_treatments) {
                  if ($cancer_treatments=="Yes") 
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                  else
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                  if ($i==6 || $i==7) {
                    echo '<td class="row-gray results-column hide '.$i.'"><img src="'.$src.'" ></td>';
                  } else{
                    echo '<td class="row-gray results-column '.$i.'"><img src="'.$src.'" ></td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Surgery for cataracts, as well as other eye lens related hospital admissions.">Eye surgery</td>
                <?php $i=1;
                foreach ($fundDetailsArr['eye_surgery'] as $eye_surgery_key => $eye_surgery ) {
                  if ($eye_surgery=="Yes") 
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                  else
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                  if ($i==6 || $i==7) {
                    echo '<td class="row-light-blue results-column hide '.$i.'"><img src="'.$src.'" ></td>';
                  } else{
                    echo '<td class="row-light-blue results-column '.$i.'"><img src="'.$src.'" ></td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Medical and surgical admissions intended to investigate, diagnose, monitor and/or treat heart-related conditions. May include services such as open heart and bypass surgery, and invasive cardiac investigations and procedures such as angiograms, angioplasties and stent insertions.">Heart surgery</td>
                <?php $i=1;
                foreach ($fundDetailsArr['heart_surgery'] as $heart_surgery_key => $heart_surgery ) {
                  if ($heart_surgery=="Yes") 
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                  else
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                  if ($i==6 || $i==7) {
                    echo '<td class="row-gray results-column hide '.$i.'"><img src="'.$src.'" ></td>';
                  } else{
                    echo '<td class="row-gray results-column '.$i.'"><img src="'.$src.'" ></td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Admission to hospital for medication and possible removal of an inflamed appendix (appendicitis).">Appendicitis treatment</td>
                <?php $i=1;
                foreach ($fundDetailsArr['appendicitis_treatment'] as $appendicitis_treatment_key => $appendicitis_treatment ) {
                  if ($appendicitis_treatment=="Yes") 
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                  else
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                  if ($i==6 || $i==7) {
                    echo '<td class="row-light-blue results-column hide '.$i.'"><img src="'.$src.'" ></td>';
                  } else{
                    echo '<td class="row-light-blue results-column '.$i.'"><img src="'.$src.'" ></td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="A gastroscopy is performed to investigate or diagnose swallowing, stomach pain and ulcers.">Gastroscopies</td>
                <?php $i=1;
                foreach ($fundDetailsArr['gastroscopies'] as $gastroscopies_key => $gastroscopies ) {
                  if ($gastroscopies=="Yes") 
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                  else
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                  if ($i==6 || $i==7) {
                    echo '<td class="row-gray results-column hide '.$i.'"><img src="'.$src.'" ></td>';
                  } else{
                    echo '<td class="row-gray results-column '.$i.'"><img src="'.$src.'" ></td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Treatment aimed at assisting or replacing the function of the kidneys by ensuring the appropriate balance of chemicals in the blood. It can include both haemodialysis (circulating the blood through a machine) and peritoneal dialysis (infusing and draining a sterile solution into the abdomen).">Kidney failure treatment</td>
                <?php $i=1;
                foreach ($fundDetailsArr['kidney_failure_treatment'] as $kidney_failure_treatment_key => $kidney_failure_treatment ) {
                  if ($kidney_failure_treatment=="Yes") 
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                  else
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                  if ($i==6 || $i==7) {
                    echo '<td class="row-light-blue results-column hide '.$i.'"><img src="'.$src.'" ></td>';
                  } else{
                    echo '<td class="row-light-blue results-column '.$i.'"><img src="'.$src.'" ></td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Therapy that assists in recovery following a major health event, such as after a joint replacement or following a heart attack. It must be provided at an approved rehabilitation facility and under an approved program.">Rehabilitation treatment</td>
                <?php $i=1;
                foreach ($fundDetailsArr['rehabilitation_treatment'] as $rehabilitation_treatment_key => $rehabilitation_treatment ) {
                  if ($rehabilitation_treatment=="Yes") 
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                  else
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                  if ($i==6 || $i==7) {
                    echo '<td class="row-gray results-column hide '.$i.'"><img src="'.$src.'" ></td>';
                  } else{
                    echo '<td class="row-gray results-column '.$i.'"><img src="'.$src.'" ></td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="In Vitro Fertilisation (IVF) is an assisted reproductive technologies (ART) used to help infertile couple to conceive a child.">Fertility treatment (IVF)</td>
                <?php $i=1;
                foreach ($fundDetailsArr['fertility_treatment'] as $fertility_treatment_key => $fertility_treatment ) {
                  if ($fertility_treatment=="Yes") 
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                  else
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                  if ($i==6 || $i==7) {
                    echo '<td class="row-light-blue results-column hide '.$i.'"><img src="'.$src.'" ></td>';
                  } else{
                    echo '<td class="row-light-blue results-column '.$i.'"><img src="'.$src.'" ></td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Surgery to improve your appearance that is not medically necessary.">Cosmetic surgery</td>
                <?php $i=1;
                foreach ($fundDetailsArr['cosmetic_surgery'] as $cosmetic_surgery_key => $cosmetic_surgery ) {
                  if ($cosmetic_surgery=="Yes") 
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                  else
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                  if ($i==6 || $i==7) {
                    echo '<td class="row-gray results-column hide '.$i.'"><img src="'.$src.'" ></td>';
                  } else{
                    echo '<td class="row-gray results-column '.$i.'"><img src="'.$src.'" ></td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="The Medicare Benefits Schedule (MBS) lists all the medical services subsidised by the Australian government through Medicare. This includes thousands of in-hospital services that we may pay benefits towards.">All other in-hospital services where a Medicare benefit is payable</td>
                <?php $i=1;
                foreach ($fundDetailsArr['other_hospital_services'] as $other_hospital_services_key => $other_hospital_services ) {
                  if ($other_hospital_services=="Yes") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "";
                  }
                  else if ($other_hospital_services=="Yes | 100% of MBS") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "100% of MBS";
                  }
                  else{
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                    $text = "";
                  }
                  if ($i==6 || $i==7) {
                    echo '<td class="row-light-blue results-column hide '.$i.'"><img src="'.$src.'" ><br>'.$text.'</td>';
                  } else{
                    echo '<td class="row-light-blue results-column '.$i.'"><img src="'.$src.'" ><br>'.$text.'</td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td class="row-gray policy_document">Policy Document</td>
                <?php $i=1;
                foreach ($fundDetailsArr['policy_document'] as $policy_document_key => $policy_document ) {
                  if ($i==6 || $i==7) {
                    echo '<td class="row-gray policy_document results-column pdf hide '.$i.'"><a href="'.$policy_document.'" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>';
                  } else{
                    echo '<td class="row-gray policy_document results-column pdf '.$i.'"><a href="'.$policy_document.'" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>';
                  }
                  $i++;
                } ?>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <?php $i=1;
                foreach ($fundDetailsArr['weekly_premimum_price'] as $weekly_premimum_price_key => $weekly_premimum_price) {
                  if ($i==6 || $i==7) {
                    echo '<td class="row-light-blue results-column hide '.$i.'"><br><a href="javascript:void(0);" data-url="'.$fundDetailsArr['policy_url'][$weekly_premimum_price_key].'" name="buy_now" class="buy-now-btn">Select Now</a><br><br></td>';
                  } else{
                    echo '<td class="row-light-blue results-column '.$i.'"><br><a href="javascript:void(0);" data-url="'.$fundDetailsArr['policy_url'][$weekly_premimum_price_key].'" name="buy_now" class="buy-now-btn">Select Now</a><br><br></td>';
                  }
                  $i++;
                } ?>
              </tr>
            </table>
            <table class="nav-table">
              <tr>
                <td class="previous-item"><button type="button" name="previous-btn" class="previous-btn nav-btn"><img class="img-responsive prev" src="<?php echo get_stylesheet_directory_uri() ?>/images/prev.png">&nbsp;&nbsp;&nbsp;Prev</button></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td class="next-item"><button type="button" name="next-btn" class="next-btn nav-btn">Next&nbsp;&nbsp;&nbsp;<img class="img-responsive next" src="<?php echo get_stylesheet_directory_uri() ?>/images/next.png"></button></td>
              </tr>
            </table>
            <p>&nbsp;</p>
          </div>
        </div>
      </div>
      <div class="hidden-lg hidden-md hidden-sm col-xs-12">
        <div class="col-lg-12 col-md-12">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <table class="result-table">
              <tr>
                <td colspan="2" class="text-center"><img src="<?php echo $fundDetailsArr['policy_image_url'][0]; ?>"></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue fund-name fixed_head"><span class="policy-company"><?php echo $fundDetailsArr['policy_name'][0]; ?></span><img class="right-arrow" src="<?php echo get_stylesheet_directory_uri() ?>/images/right_arrow.png"></td>
              </tr>
              <tr>
                <td colspan="2" class="row-light-gray"><p class="premium">$<?php echo $fundDetailsArr['weekly_premimum_price'][0]; ?></p><p class="premium-type">Weekly</p><a href="javascript:void(0);" data-url="<?php echo $fundDetailsArr['policy_url'][0] ?>" name="buy_now" class="buy-now-btn">Select Now</a><br><br></td>
              </tr>
              <!-- <tr>
                <td colspan="2" class="row-dark-blue row-header">General</td>
              </tr>
              <tr>
                <td class="row-gray">Level of cover</td>
                <td class="row-gray"><?php echo $fundDetailsArr['level_of_cover'][0]; ?></td>
              </tr> -->
              <tr>
                <td class="row-light-blue">About this cover</td>
                <td class="row-light-blue"><?php echo $fundDetailsArr['about_this_cover'][0]; ?></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">Visa Compliance</td>
                </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Does this health insurance cover meeds the condition of the Australian Department of Immigration and Border Protection's visa requirements.">Meets government requirements</td>
                <?php 
                if ($fundDetailsArr['meets_govt_requirements'][0]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>"></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Some insurance will immediately issue a letter confirming your health insurance. You can use this letter as a proof of insurance with your visa application.">Instant visa letter issued</td>
                <?php 
                if ($fundDetailsArr['instant_visa_letter_issued'][0]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>"></td>
              </tr>
              <tr><td class="row-gray">Visa types</td>
                <td class="row-gray"><?php echo $fundDetailsArr['visa_types'][0]; ?></td>
                </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">What's Included</td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Helps towards the hospital accommodation costs and doctors, specialists, surgeons and anaesthetists fees for the hospital services (see some example services below).">Hospital expenses</td>
                <?php 
                if ($fundDetailsArr['hospital_expenses'][0]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="When you’re treated in hospital by a doctor, surgeon or anaesthetist the set fee for their service is called the Medicare Benefits Schedule (MBS) fee. If the person treating you charges only the MBS fee you won’t need to pay anything. If they choose to charge above the MBS fee you will likely have to pay an out-of-pocket cost to cover the difference.">Medical expenses (in hospital)</td>
                <?php 
                  if ($fundDetailsArr['medical_expenses_inhospital'][0]=="Yes") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "";
                  }
                  else if ($fundDetailsArr['medical_expenses_inhospital'][0]=="Yes | 100% of MBS") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "100% of MBS";
                  }
                  else{
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                    $text = "";
                  } 
                ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ><br><?php echo $text; ?></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Admission into private hospital. Generally overnight accommodation in a private or shared room, same day admissions, intensive care, operating theatre fees, etc.">Private hospital admission</td>
                <?php 
                if ($fundDetailsArr['private_hospital_admission'][0]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Admission into public hospital as a private patient. Generally overnight accommodation in a private or shared room, same day admissions, emergency department facility fees (if the service leads to an admission or it is continuation of care following an admission)">Public hospital admission</td>
                <?php 
                if ($fundDetailsArr['private_hospital_admission'][0]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Any accidential injury. For accident to be covered, treatment must be sought through a Doctor or an Emergency Department within 48 hours of sustaining the injury.">Accidents & emergency services</td>
                <?php 
                if ($fundDetailsArr['accidents_and_emergency_services'][0]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="When you needed an ambulance to be transported to hospital, health insurance providers will pay 100% cover for when you need an emergency ambulance services.">Emergency Ambulance</td>
                <?php 
                if ($fundDetailsArr['emergency_ambulance'][0]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="If someone on your membership has to return to your home country due to a substantial life-altering illness or injury, health providers may arrange and pay the reasonable cost of return travel with the appropriate medical supervision.">Repatriation to home country</td>
                <?php 
                if ($fundDetailsArr['repartation_to_home_country'][0]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">Excesses</td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="This is the amount you pay towards your hospital treatment before health insurance providers make any benefits. It's paid once per person per calendar year. It does not apply to children on a family policy.">Excess option</td>
                <td class="row-gray"><?php echo $fundDetailsArr['excess_option'][0]; ?></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">Some examples of what services are covered</td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="When you needed an ambulance to be transported to hospital, health insurance providers will pay 100% cover for when you need an emergency ambulance services.">Ambulance services</td>
                <?php 
                if ($fundDetailsArr['ambulance_services'][0]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Any accidential injury. For accident to be covered, treatment must be sought through a Doctor or an Emergency Department within 48 hours of sustaining the injury.">Accidential injury</td>
                <?php 
                if ($fundDetailsArr['accidential_injury'][0]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Services and treatment provided to an admitted patient in hospital that deal with the care of women during pregnancy, childbirth and following delivery. Includes regular and caesarean childbirth and in-patient diagnostic imaging, as well as treatment of pregnancy related complications.">Pregnancy and related services</td>
                <?php 
                if ($fundDetailsArr['pregnancy_and_related_services'][0]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Reconstructions to repair ligament tears, remove loose tissue and to treat other damage. Surgery to replace a hip or knee joint with a prosthesis. Its aim is to relieve pain and/or increase function by replacing all or part of the joint resurface.">Joint replacements surgery (hip, knee, shoulder)</td>
                <?php 
                if ($fundDetailsArr['join_replacements_surgery'][0]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="A procedure to examine the lining of the large bowel (colon). It is used to investigate the cause of abdominal pain, bleeding, irregular bowel habits, to remove polyps or detect cancer.">Colonoscopy</td>
                <?php 
                if ($fundDetailsArr['colonoscopy'][0]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="There are 200 different type of cancers and most area of the body can be affected. Some health insurance providers will cover some or all cancer treatments.">Cancer treatments</td>
                <?php 
                if ($fundDetailsArr['cancer_treatments'][0]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Surgery for cataracts, as well as other eye lens related hospital admissions.">Eye surgery</td>
                <?php 
                if ($fundDetailsArr['eye_surgery'][0]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Medical and surgical admissions intended to investigate, diagnose, monitor and/or treat heart-related conditions. May include services such as open heart and bypass surgery, and invasive cardiac investigations and procedures such as angiograms, angioplasties and stent insertions.">Heart surgery</td>
                <?php 
                if ($fundDetailsArr['heart_surgery'][0]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Admission to hospital for medication and possible removal of an inflamed appendix (appendicitis).">Appendicitis treatment</td>
                <?php 
                if ($fundDetailsArr['appendicitis_treatment'][0]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="A gastroscopy is performed to investigate or diagnose swallowing, stomach pain and ulcers.">Gastroscopies</td>
                <?php 
                if ($fundDetailsArr['gastroscopies'][0]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Treatment aimed at assisting or replacing the function of the kidneys by ensuring the appropriate balance of chemicals in the blood. It can include both haemodialysis (circulating the blood through a machine) and peritoneal dialysis (infusing and draining a sterile solution into the abdomen).">Kidney failure treatment</td>
                <?php 
                if ($fundDetailsArr['kidney_failure_treatment'][0]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Therapy that assists in recovery following a major health event, such as after a joint replacement or following a heart attack. It must be provided at an approved rehabilitation facility and under an approved program.">Rehabilitation treatment</td>
                <?php 
                if ($fundDetailsArr['rehabilitation_treatment'][0]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="In Vitro Fertilisation (IVF) is an assisted reproductive technologies (ART) used to help infertile couple to conceive a child.">Fertility treatment (IVF)</td>
                <?php 
                if ($fundDetailsArr['fertility_treatment'][0]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Surgery to improve your appearance that is not medically necessary.">Cosmetic surgery</td>
                 <?php 
                if ($fundDetailsArr['cosmetic_surgery'][0]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="The Medicare Benefits Schedule (MBS) lists all the medical services subsidised by the Australian government through Medicare. This includes thousands of in-hospital services that we may pay benefits towards.">All other in-hospital services where a Medicare benefit is payable</td>
                <?php 
                  if ($fundDetailsArr['other_hospital_services'][0]=="Yes") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "";
                  }
                  else if ($fundDetailsArr['other_hospital_services'][0]=="Yes | 100% of MBS") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "100% of MBS";
                  }
                  else{
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                    $text = "";
                  } 
                ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ><br><?php echo $text; ?></td>
              </tr>
              <tr>
                <td class="row-gray policy_document">Policy Document</td>
                <td class="row-gray policy_document pdf"><a href="<?php echo $fundDetailsArr['policy_document'][0]; ?>" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
              </tr>
              <tr>
                <td colspan="2" class="row-light-blue"><br><a href="javascript:void(0);" data-url="<?php echo $fundDetailsArr['policy_url'][0] ?>" name="buy_now" class="buy-now-btn">Select Now</a><br><br></td>
              </tr>
            </table>
            <table class="result-table hide">
              <tr>
                <td colspan="2" class="text-center"><img src="<?php echo $fundDetailsArr['policy_image_url'][1]; ?>"></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue fund-name fixed_head"><img class="left-arrow" src="<?php echo get_stylesheet_directory_uri() ?>/images/left_arrow.png"><span class="policy-company"><?php echo $fundDetailsArr['policy_name'][1]; ?></span><img class="right-arrow" src="<?php echo get_stylesheet_directory_uri() ?>/images/right_arrow.png"></td>
              </tr>
              <tr>
                <td colspan="2" class="row-light-gray"><p class="premium">$<?php echo $fundDetailsArr['weekly_premimum_price'][1]; ?></p><p class="premium-type">Weekly</p><a href="javascript:void(0);" data-url="<?php echo $fundDetailsArr['policy_url'][1] ?>" name="buy_now" class="buy-now-btn">Select Now</a><br><br></td>
              </tr>
              <!-- <tr>
                <td colspan="2" class="row-dark-blue row-header">General</td>
              </tr>
              <tr>
                <td class="row-gray">Level of cover</td>
                <td class="row-gray"><?php echo $fundDetailsArr['level_of_cover'][1]; ?></td>
              </tr> -->
              <tr>
                <td class="row-light-blue">About this cover</td>
                <td class="row-light-blue"><?php echo $fundDetailsArr['about_this_cover'][1]; ?></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">Visa Compliance</td>
                </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Does this health insurance cover meeds the condition of the Australian Department of Immigration and Border Protection's visa requirements.">Meets government requirements</td>
                <?php 
                if ($fundDetailsArr['meets_govt_requirements'][1]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>"></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Some insurance will immediately issue a letter confirming your health insurance. You can use this letter as a proof of insurance with your visa application.">Instant visa letter issued</td>
                <?php 
                if ($fundDetailsArr['instant_visa_letter_issued'][1]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>"></td>
              </tr>
              <tr><td class="row-gray">Visa types</td>
                <td class="row-gray"><?php echo $fundDetailsArr['visa_types'][1]; ?></td>
                </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">What's Included</td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Helps towards the hospital accommodation costs and doctors, specialists, surgeons and anaesthetists fees for the hospital services (see some example services below).">Hospital expenses</td>
                <?php 
                if ($fundDetailsArr['hospital_expenses'][1]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="When you’re treated in hospital by a doctor, surgeon or anaesthetist the set fee for their service is called the Medicare Benefits Schedule (MBS) fee. If the person treating you charges only the MBS fee you won’t need to pay anything. If they choose to charge above the MBS fee you will likely have to pay an out-of-pocket cost to cover the difference.">Medical expenses (in hospital)</td>
                <?php 
                  if ($fundDetailsArr['medical_expenses_inhospital'][1]=="Yes") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "";
                  }
                  else if ($fundDetailsArr['medical_expenses_inhospital'][1]=="Yes | 100% of MBS") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "100% of MBS";
                  }
                  else{
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                    $text = "";
                  } 
                ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ><br><?php echo $text; ?></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Admission into private hospital. Generally overnight accommodation in a private or shared room, same day admissions, intensive care, operating theatre fees, etc.">Private hospital admission</td>
                <?php 
                if ($fundDetailsArr['private_hospital_admission'][1]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Admission into public hospital as a private patient. Generally overnight accommodation in a private or shared room, same day admissions, emergency department facility fees (if the service leads to an admission or it is continuation of care following an admission)">Public hospital admission</td>
                <?php 
                if ($fundDetailsArr['private_hospital_admission'][1]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Any accidential injury. For accident to be covered, treatment must be sought through a Doctor or an Emergency Department within 48 hours of sustaining the injury.">Accidents & emergency services</td>
                <?php 
                if ($fundDetailsArr['accidents_and_emergency_services'][1]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="When you needed an ambulance to be transported to hospital, health insurance providers will pay 100% cover for when you need an emergency ambulance services.">Emergency Ambulance</td>
                <?php 
                if ($fundDetailsArr['emergency_ambulance'][1]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="If someone on your membership has to return to your home country due to a substantial life-altering illness or injury, health providers may arrange and pay the reasonable cost of return travel with the appropriate medical supervision.">Repatriation to home country</td>
                <?php 
                if ($fundDetailsArr['repartation_to_home_country'][1]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">Excesses</td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="This is the amount you pay towards your hospital treatment before health insurance providers make any benefits. It's paid once per person per calendar year. It does not apply to children on a family policy.">Excess option</td>
                <td class="row-gray"><?php echo $fundDetailsArr['excess_option'][1]; ?></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">Some examples of what services are covered</td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="When you needed an ambulance to be transported to hospital, health insurance providers will pay 100% cover for when you need an emergency ambulance services.">Ambulance services</td>
                <?php 
                if ($fundDetailsArr['ambulance_services'][1]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Any accidential injury. For accident to be covered, treatment must be sought through a Doctor or an Emergency Department within 48 hours of sustaining the injury.">Accidential injury</td>
                <?php 
                if ($fundDetailsArr['accidential_injury'][1]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Services and treatment provided to an admitted patient in hospital that deal with the care of women during pregnancy, childbirth and following delivery. Includes regular and caesarean childbirth and in-patient diagnostic imaging, as well as treatment of pregnancy related complications.">Pregnancy and related services</td>
                <?php 
                if ($fundDetailsArr['pregnancy_and_related_services'][1]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Reconstructions to repair ligament tears, remove loose tissue and to treat other damage. Surgery to replace a hip or knee joint with a prosthesis. Its aim is to relieve pain and/or increase function by replacing all or part of the joint resurface.">Joint replacements surgery (hip, knee, shoulder)</td>
                <?php 
                if ($fundDetailsArr['join_replacements_surgery'][1]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="A procedure to examine the lining of the large bowel (colon). It is used to investigate the cause of abdominal pain, bleeding, irregular bowel habits, to remove polyps or detect cancer.">Colonoscopy</td>
                <?php 
                if ($fundDetailsArr['colonoscopy'][1]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="There are 200 different type of cancers and most area of the body can be affected. Some health insurance providers will cover some or all cancer treatments.">Cancer treatments</td>
                <?php 
                if ($fundDetailsArr['cancer_treatments'][1]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Surgery for cataracts, as well as other eye lens related hospital admissions.">Eye surgery</td>
                <?php 
                if ($fundDetailsArr['eye_surgery'][1]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Medical and surgical admissions intended to investigate, diagnose, monitor and/or treat heart-related conditions. May include services such as open heart and bypass surgery, and invasive cardiac investigations and procedures such as angiograms, angioplasties and stent insertions.">Heart surgery</td>
                <?php 
                if ($fundDetailsArr['heart_surgery'][1]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Admission to hospital for medication and possible removal of an inflamed appendix (appendicitis).">Appendicitis treatment</td>
                <?php 
                if ($fundDetailsArr['appendicitis_treatment'][1]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="A gastroscopy is performed to investigate or diagnose swallowing, stomach pain and ulcers.">Gastroscopies</td>
                <?php 
                if ($fundDetailsArr['gastroscopies'][1]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Treatment aimed at assisting or replacing the function of the kidneys by ensuring the appropriate balance of chemicals in the blood. It can include both haemodialysis (circulating the blood through a machine) and peritoneal dialysis (infusing and draining a sterile solution into the abdomen).">Kidney failure treatment</td>
                <?php 
                if ($fundDetailsArr['kidney_failure_treatment'][1]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Therapy that assists in recovery following a major health event, such as after a joint replacement or following a heart attack. It must be provided at an approved rehabilitation facility and under an approved program.">Rehabilitation treatment</td>
                <?php 
                if ($fundDetailsArr['rehabilitation_treatment'][1]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="In Vitro Fertilisation (IVF) is an assisted reproductive technologies (ART) used to help infertile couple to conceive a child.">Fertility treatment (IVF)</td>
                <?php 
                if ($fundDetailsArr['fertility_treatment'][1]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Surgery to improve your appearance that is not medically necessary.">Cosmetic surgery</td>
                 <?php 
                if ($fundDetailsArr['cosmetic_surgery'][1]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="The Medicare Benefits Schedule (MBS) lists all the medical services subsidised by the Australian government through Medicare. This includes thousands of in-hospital services that we may pay benefits towards.">All other in-hospital services where a Medicare benefit is payable</td>
                <?php 
                  if ($fundDetailsArr['other_hospital_services'][1]=="Yes") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "";
                  }
                  else if ($fundDetailsArr['other_hospital_services'][1]=="Yes | 100% of MBS") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "100% of MBS";
                  }
                  else{
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                    $text = "";
                  } 
                ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ><br><?php echo $text; ?></td>
              </tr>
              <tr>
                <td class="row-gray policy_document">Policy Document</td>
                <td class="row-gray policy_document pdf"><a href="<?php echo $fundDetailsArr['policy_document'][1]; ?>" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
              </tr>
              <tr>
                <td colspan="2" class="row-light-blue"><br><a href="javascript:void(0);" data-url="<?php echo $fundDetailsArr['policy_url'][1] ?>" name="buy_now" class="buy-now-btn">Select Now</a><br><br></td>
              </tr>
            </table>
            <table class="result-table hide">
              <tr>
                <td colspan="2" class="text-center"><img src="<?php echo $fundDetailsArr['policy_image_url'][2]; ?>"></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue fund-name fixed_head"><img class="left-arrow" src="<?php echo get_stylesheet_directory_uri() ?>/images/left_arrow.png"><span class="policy-company"><?php echo $fundDetailsArr['policy_name'][2]; ?></span><img class="right-arrow" src="<?php echo get_stylesheet_directory_uri() ?>/images/right_arrow.png"></td>
              </tr>
              <tr>
                <td colspan="2" class="row-light-gray"><p class="premium">$<?php echo $fundDetailsArr['weekly_premimum_price'][2]; ?></p><p class="premium-type">Weekly</p><a href="javascript:void(0);" data-url="<?php echo $fundDetailsArr['policy_url'][2] ?>" name="buy_now" class="buy-now-btn">Select Now</a><br><br></td>
              </tr>
              <!-- <tr>
                <td colspan="2" class="row-dark-blue row-header">General</td>
              </tr>
              <tr>
                <td class="row-gray">Level of cover</td>
                <td class="row-gray"><?php echo $fundDetailsArr['level_of_cover'][2]; ?></td>
              </tr> -->
              <tr>
                <td class="row-light-blue">About this cover</td>
                <td class="row-light-blue"><?php echo $fundDetailsArr['about_this_cover'][2]; ?></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">Visa Compliance</td>
                </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Does this health insurance cover meeds the condition of the Australian Department of Immigration and Border Protection's visa requirements.">Meets government requirements</td>
                <?php 
                if ($fundDetailsArr['meets_govt_requirements'][2]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>"></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Some insurance will immediately issue a letter confirming your health insurance. You can use this letter as a proof of insurance with your visa application.">Instant visa letter issued</td>
                <?php 
                if ($fundDetailsArr['instant_visa_letter_issued'][2]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>"></td>
              </tr>
              <tr><td class="row-gray">Visa types</td>
                <td class="row-gray"><?php echo $fundDetailsArr['visa_types'][2]; ?></td>
                </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">What's Included</td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Helps towards the hospital accommodation costs and doctors, specialists, surgeons and anaesthetists fees for the hospital services (see some example services below).">Hospital expenses</td>
                <?php 
                if ($fundDetailsArr['hospital_expenses'][2]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="When you’re treated in hospital by a doctor, surgeon or anaesthetist the set fee for their service is called the Medicare Benefits Schedule (MBS) fee. If the person treating you charges only the MBS fee you won’t need to pay anything. If they choose to charge above the MBS fee you will likely have to pay an out-of-pocket cost to cover the difference.">Medical expenses (in hospital)</td>
                <?php 
                  if ($fundDetailsArr['medical_expenses_inhospital'][2]=="Yes") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "";
                  }
                  else if ($fundDetailsArr['medical_expenses_inhospital'][2]=="Yes | 100% of MBS") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "100% of MBS";
                  }
                  else{
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                    $text = "";
                  } 
                ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ><br><?php echo $text; ?></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Admission into private hospital. Generally overnight accommodation in a private or shared room, same day admissions, intensive care, operating theatre fees, etc.">Private hospital admission</td>
                <?php 
                if ($fundDetailsArr['private_hospital_admission'][2]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Admission into public hospital as a private patient. Generally overnight accommodation in a private or shared room, same day admissions, emergency department facility fees (if the service leads to an admission or it is continuation of care following an admission)">Public hospital admission</td>
                <?php 
                if ($fundDetailsArr['private_hospital_admission'][2]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Any accidential injury. For accident to be covered, treatment must be sought through a Doctor or an Emergency Department within 48 hours of sustaining the injury.">Accidents & emergency services</td>
                <?php 
                if ($fundDetailsArr['accidents_and_emergency_services'][2]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="When you needed an ambulance to be transported to hospital, health insurance providers will pay 100% cover for when you need an emergency ambulance services.">Emergency Ambulance</td>
                <?php 
                if ($fundDetailsArr['emergency_ambulance'][2]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="If someone on your membership has to return to your home country due to a substantial life-altering illness or injury, health providers may arrange and pay the reasonable cost of return travel with the appropriate medical supervision.">Repatriation to home country</td>
                <?php 
                if ($fundDetailsArr['repartation_to_home_country'][2]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">Excesses</td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="This is the amount you pay towards your hospital treatment before health insurance providers make any benefits. It's paid once per person per calendar year. It does not apply to children on a family policy.">Excess option</td>
                <td class="row-gray"><?php echo $fundDetailsArr['excess_option'][2]; ?></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">Some examples of what services are covered</td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="When you needed an ambulance to be transported to hospital, health insurance providers will pay 100% cover for when you need an emergency ambulance services.">Ambulance services</td>
                <?php 
                if ($fundDetailsArr['ambulance_services'][2]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Any accidential injury. For accident to be covered, treatment must be sought through a Doctor or an Emergency Department within 48 hours of sustaining the injury.">Accidential injury</td>
                <?php 
                if ($fundDetailsArr['accidential_injury'][2]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Services and treatment provided to an admitted patient in hospital that deal with the care of women during pregnancy, childbirth and following delivery. Includes regular and caesarean childbirth and in-patient diagnostic imaging, as well as treatment of pregnancy related complications.">Pregnancy and related services</td>
                <?php 
                if ($fundDetailsArr['pregnancy_and_related_services'][2]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Reconstructions to repair ligament tears, remove loose tissue and to treat other damage. Surgery to replace a hip or knee joint with a prosthesis. Its aim is to relieve pain and/or increase function by replacing all or part of the joint resurface.">Joint replacements surgery (hip, knee, shoulder)</td>
                <?php 
                if ($fundDetailsArr['join_replacements_surgery'][2]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="A procedure to examine the lining of the large bowel (colon). It is used to investigate the cause of abdominal pain, bleeding, irregular bowel habits, to remove polyps or detect cancer.">Colonoscopy</td>
                <?php 
                if ($fundDetailsArr['colonoscopy'][2]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="There are 200 different type of cancers and most area of the body can be affected. Some health insurance providers will cover some or all cancer treatments.">Cancer treatments</td>
                <?php 
                if ($fundDetailsArr['cancer_treatments'][2]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Surgery for cataracts, as well as other eye lens related hospital admissions.">Eye surgery</td>
                <?php 
                if ($fundDetailsArr['eye_surgery'][2]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Medical and surgical admissions intended to investigate, diagnose, monitor and/or treat heart-related conditions. May include services such as open heart and bypass surgery, and invasive cardiac investigations and procedures such as angiograms, angioplasties and stent insertions.">Heart surgery</td>
                <?php 
                if ($fundDetailsArr['heart_surgery'][2]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Admission to hospital for medication and possible removal of an inflamed appendix (appendicitis).">Appendicitis treatment</td>
                <?php 
                if ($fundDetailsArr['appendicitis_treatment'][2]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="A gastroscopy is performed to investigate or diagnose swallowing, stomach pain and ulcers.">Gastroscopies</td>
                <?php 
                if ($fundDetailsArr['gastroscopies'][2]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Treatment aimed at assisting or replacing the function of the kidneys by ensuring the appropriate balance of chemicals in the blood. It can include both haemodialysis (circulating the blood through a machine) and peritoneal dialysis (infusing and draining a sterile solution into the abdomen).">Kidney failure treatment</td>
                <?php 
                if ($fundDetailsArr['kidney_failure_treatment'][2]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Therapy that assists in recovery following a major health event, such as after a joint replacement or following a heart attack. It must be provided at an approved rehabilitation facility and under an approved program.">Rehabilitation treatment</td>
                <?php 
                if ($fundDetailsArr['rehabilitation_treatment'][2]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="In Vitro Fertilisation (IVF) is an assisted reproductive technologies (ART) used to help infertile couple to conceive a child.">Fertility treatment (IVF)</td>
                <?php 
                if ($fundDetailsArr['fertility_treatment'][2]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Surgery to improve your appearance that is not medically necessary.">Cosmetic surgery</td>
                 <?php 
                if ($fundDetailsArr['cosmetic_surgery'][2]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="The Medicare Benefits Schedule (MBS) lists all the medical services subsidised by the Australian government through Medicare. This includes thousands of in-hospital services that we may pay benefits towards.">All other in-hospital services where a Medicare benefit is payable</td>
                <?php 
                  if ($fundDetailsArr['other_hospital_services'][2]=="Yes") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "";
                  }
                  else if ($fundDetailsArr['other_hospital_services'][2]=="Yes | 100% of MBS") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "100% of MBS";
                  }
                  else{
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                    $text = "";
                  } 
                ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ><br><?php echo $text; ?></td>
              </tr>
              <tr>
                <td class="row-gray policy_document">Policy Document</td>
                <td class="row-gray policy_document pdf"><a href="<?php echo $fundDetailsArr['policy_document'][2]; ?>" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
              </tr>
              <tr>
                <td colspan="2" class="row-light-blue"><br><a href="javascript:void(0);" data-url="<?php echo $fundDetailsArr['policy_url'][2] ?>" name="buy_now" class="buy-now-btn">Select Now</a><br><br></td>
              </tr>
            </table>
            <table class="result-table hide">
              <tr>
                <td colspan="2" class="text-center"><img src="<?php echo $fundDetailsArr['policy_image_url'][3]; ?>"></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue fund-name fixed_head"><img class="left-arrow" src="<?php echo get_stylesheet_directory_uri() ?>/images/left_arrow.png"><span class="policy-company"><?php echo $fundDetailsArr['policy_name'][3]; ?></span><img class="right-arrow" src="<?php echo get_stylesheet_directory_uri() ?>/images/right_arrow.png"></td>
              </tr>
              <tr>
                <td colspan="2" class="row-light-gray"><p class="premium">$<?php echo $fundDetailsArr['weekly_premimum_price'][3]; ?></p><p class="premium-type">Weekly</p><a href="javascript:void(0);" data-url="<?php echo $fundDetailsArr['policy_url'][3] ?>" name="buy_now" class="buy-now-btn">Select Now</a><br><br></td>
              </tr>
              <!-- <tr>
                <td colspan="2" class="row-dark-blue row-header">General</td>
              </tr>
              <tr>
                <td class="row-gray">Level of cover</td>
                <td class="row-gray"><?php echo $fundDetailsArr['level_of_cover'][3]; ?></td>
              </tr> -->
              <tr>
                <td class="row-light-blue">About this cover</td>
                <td class="row-light-blue"><?php echo $fundDetailsArr['about_this_cover'][3]; ?></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">Visa Compliance</td>
                </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Does this health insurance cover meeds the condition of the Australian Department of Immigration and Border Protection's visa requirements.">Meets government requirements</td>
                <?php 
                if ($fundDetailsArr['meets_govt_requirements'][3]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>"></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Some insurance will immediately issue a letter confirming your health insurance. You can use this letter as a proof of insurance with your visa application.">Instant visa letter issued</td>
                <?php 
                if ($fundDetailsArr['instant_visa_letter_issued'][3]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>"></td>
              </tr>
              <tr><td class="row-gray">Visa types</td>
                <td class="row-gray"><?php echo $fundDetailsArr['visa_types'][3]; ?></td>
                </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">What's Included</td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Helps towards the hospital accommodation costs and doctors, specialists, surgeons and anaesthetists fees for the hospital services (see some example services below).">Hospital expenses</td>
                <?php 
                if ($fundDetailsArr['hospital_expenses'][3]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="When you’re treated in hospital by a doctor, surgeon or anaesthetist the set fee for their service is called the Medicare Benefits Schedule (MBS) fee. If the person treating you charges only the MBS fee you won’t need to pay anything. If they choose to charge above the MBS fee you will likely have to pay an out-of-pocket cost to cover the difference.">Medical expenses (in hospital)</td>
                <?php 
                  if ($fundDetailsArr['medical_expenses_inhospital'][3]=="Yes") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "";
                  }
                  else if ($fundDetailsArr['medical_expenses_inhospital'][3]=="Yes | 100% of MBS") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "100% of MBS";
                  }
                  else{
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                    $text = "";
                  } 
                ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ><br><?php echo $text; ?></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Admission into private hospital. Generally overnight accommodation in a private or shared room, same day admissions, intensive care, operating theatre fees, etc.">Private hospital admission</td>
                <?php 
                if ($fundDetailsArr['private_hospital_admission'][3]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Admission into public hospital as a private patient. Generally overnight accommodation in a private or shared room, same day admissions, emergency department facility fees (if the service leads to an admission or it is continuation of care following an admission)">Public hospital admission</td>
                <?php 
                if ($fundDetailsArr['private_hospital_admission'][3]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Any accidential injury. For accident to be covered, treatment must be sought through a Doctor or an Emergency Department within 48 hours of sustaining the injury.">Accidents & emergency services</td>
                <?php 
                if ($fundDetailsArr['accidents_and_emergency_services'][3]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="When you needed an ambulance to be transported to hospital, health insurance providers will pay 100% cover for when you need an emergency ambulance services.">Emergency Ambulance</td>
                <?php 
                if ($fundDetailsArr['emergency_ambulance'][3]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="If someone on your membership has to return to your home country due to a substantial life-altering illness or injury, health providers may arrange and pay the reasonable cost of return travel with the appropriate medical supervision.">Repatriation to home country</td>
                <?php 
                if ($fundDetailsArr['repartation_to_home_country'][3]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">Excesses</td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="This is the amount you pay towards your hospital treatment before health insurance providers make any benefits. It's paid once per person per calendar year. It does not apply to children on a family policy.">Excess option</td>
                <td class="row-gray"><?php echo $fundDetailsArr['excess_option'][3]; ?></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">Some examples of what services are covered</td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="When you needed an ambulance to be transported to hospital, health insurance providers will pay 100% cover for when you need an emergency ambulance services.">Ambulance services</td>
                <?php 
                if ($fundDetailsArr['ambulance_services'][3]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Any accidential injury. For accident to be covered, treatment must be sought through a Doctor or an Emergency Department within 48 hours of sustaining the injury.">Accidential injury</td>
                <?php 
                if ($fundDetailsArr['accidential_injury'][3]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Services and treatment provided to an admitted patient in hospital that deal with the care of women during pregnancy, childbirth and following delivery. Includes regular and caesarean childbirth and in-patient diagnostic imaging, as well as treatment of pregnancy related complications.">Pregnancy and related services</td>
                <?php 
                if ($fundDetailsArr['pregnancy_and_related_services'][3]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Reconstructions to repair ligament tears, remove loose tissue and to treat other damage. Surgery to replace a hip or knee joint with a prosthesis. Its aim is to relieve pain and/or increase function by replacing all or part of the joint resurface.">Joint replacements surgery (hip, knee, shoulder)</td>
                <?php 
                if ($fundDetailsArr['join_replacements_surgery'][3]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="A procedure to examine the lining of the large bowel (colon). It is used to investigate the cause of abdominal pain, bleeding, irregular bowel habits, to remove polyps or detect cancer.">Colonoscopy</td>
                <?php 
                if ($fundDetailsArr['colonoscopy'][3]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="There are 200 different type of cancers and most area of the body can be affected. Some health insurance providers will cover some or all cancer treatments.">Cancer treatments</td>
                <?php 
                if ($fundDetailsArr['cancer_treatments'][3]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Surgery for cataracts, as well as other eye lens related hospital admissions.">Eye surgery</td>
                <?php 
                if ($fundDetailsArr['eye_surgery'][3]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Medical and surgical admissions intended to investigate, diagnose, monitor and/or treat heart-related conditions. May include services such as open heart and bypass surgery, and invasive cardiac investigations and procedures such as angiograms, angioplasties and stent insertions.">Heart surgery</td>
                <?php 
                if ($fundDetailsArr['heart_surgery'][3]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Admission to hospital for medication and possible removal of an inflamed appendix (appendicitis).">Appendicitis treatment</td>
                <?php 
                if ($fundDetailsArr['appendicitis_treatment'][3]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="A gastroscopy is performed to investigate or diagnose swallowing, stomach pain and ulcers.">Gastroscopies</td>
                <?php 
                if ($fundDetailsArr['gastroscopies'][3]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Treatment aimed at assisting or replacing the function of the kidneys by ensuring the appropriate balance of chemicals in the blood. It can include both haemodialysis (circulating the blood through a machine) and peritoneal dialysis (infusing and draining a sterile solution into the abdomen).">Kidney failure treatment</td>
                <?php 
                if ($fundDetailsArr['kidney_failure_treatment'][3]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Therapy that assists in recovery following a major health event, such as after a joint replacement or following a heart attack. It must be provided at an approved rehabilitation facility and under an approved program.">Rehabilitation treatment</td>
                <?php 
                if ($fundDetailsArr['rehabilitation_treatment'][3]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="In Vitro Fertilisation (IVF) is an assisted reproductive technologies (ART) used to help infertile couple to conceive a child.">Fertility treatment (IVF)</td>
                <?php 
                if ($fundDetailsArr['fertility_treatment'][3]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Surgery to improve your appearance that is not medically necessary.">Cosmetic surgery</td>
                 <?php 
                if ($fundDetailsArr['cosmetic_surgery'][3]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="The Medicare Benefits Schedule (MBS) lists all the medical services subsidised by the Australian government through Medicare. This includes thousands of in-hospital services that we may pay benefits towards.">All other in-hospital services where a Medicare benefit is payable</td>
                <?php 
                  if ($fundDetailsArr['other_hospital_services'][3]=="Yes") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "";
                  }
                  else if ($fundDetailsArr['other_hospital_services'][3]=="Yes | 100% of MBS") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "100% of MBS";
                  }
                  else{
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                    $text = "";
                  } 
                ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ><br><?php echo $text; ?></td>
              </tr>
              <tr>
                <td class="row-gray policy_document">Policy Document</td>
                <td class="row-gray policy_document pdf"><a href="<?php echo $fundDetailsArr['policy_document'][3]; ?>" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
              </tr>
              <tr>
                <td colspan="2" class="row-light-blue"><br><a href="javascript:void(0);" data-url="<?php echo $fundDetailsArr['policy_url'][3] ?>" name="buy_now" class="buy-now-btn">Select Now</a><br><br></td>
              </tr>
            </table>
            <table class="result-table hide">
              <tr>
                <td colspan="2" class="text-center"><img src="<?php echo $fundDetailsArr['policy_image_url'][4]; ?>"></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue fund-name fixed_head"><img class="left-arrow" src="<?php echo get_stylesheet_directory_uri() ?>/images/left_arrow.png"><span class="policy-company"><?php echo $fundDetailsArr['policy_name'][4]; ?></span><img class="right-arrow" src="<?php echo get_stylesheet_directory_uri() ?>/images/right_arrow.png"></td>
              </tr>
              <tr>
                <td colspan="2" class="row-light-gray"><p class="premium">$<?php echo $fundDetailsArr['weekly_premimum_price'][4]; ?></p><p class="premium-type">Weekly</p><a href="javascript:void(0);" data-url="<?php echo $fundDetailsArr['policy_url'][4] ?>" name="buy_now" class="buy-now-btn">Select Now</a><br><br></td>
              </tr>
              <!-- <tr>
                <td colspan="2" class="row-dark-blue row-header">General</td>
              </tr>
              <tr>
                <td class="row-gray">Level of cover</td>
                <td class="row-gray"><?php echo $fundDetailsArr['level_of_cover'][4]; ?></td>
              </tr> -->
              <tr>
                <td class="row-light-blue">About this cover</td>
                <td class="row-light-blue"><?php echo $fundDetailsArr['about_this_cover'][4]; ?></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">Visa Compliance</td>
                </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Does this health insurance cover meeds the condition of the Australian Department of Immigration and Border Protection's visa requirements.">Meets government requirements</td>
                <?php 
                if ($fundDetailsArr['meets_govt_requirements'][4]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>"></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Some insurance will immediately issue a letter confirming your health insurance. You can use this letter as a proof of insurance with your visa application.">Instant visa letter issued</td>
                <?php 
                if ($fundDetailsArr['instant_visa_letter_issued'][4]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>"></td>
              </tr>
              <tr><td class="row-gray">Visa types</td>
                <td class="row-gray"><?php echo $fundDetailsArr['visa_types'][4]; ?></td>
                </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">What's Included</td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Helps towards the hospital accommodation costs and doctors, specialists, surgeons and anaesthetists fees for the hospital services (see some example services below).">Hospital expenses</td>
                <?php 
                if ($fundDetailsArr['hospital_expenses'][4]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="When you’re treated in hospital by a doctor, surgeon or anaesthetist the set fee for their service is called the Medicare Benefits Schedule (MBS) fee. If the person treating you charges only the MBS fee you won’t need to pay anything. If they choose to charge above the MBS fee you will likely have to pay an out-of-pocket cost to cover the difference.">Medical expenses (in hospital)</td>
                <?php 
                  if ($fundDetailsArr['medical_expenses_inhospital'][4]=="Yes") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "";
                  }
                  else if ($fundDetailsArr['medical_expenses_inhospital'][4]=="Yes | 100% of MBS") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "100% of MBS";
                  }
                  else{
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                    $text = "";
                  } 
                ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ><br><?php echo $text; ?></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Admission into private hospital. Generally overnight accommodation in a private or shared room, same day admissions, intensive care, operating theatre fees, etc.">Private hospital admission</td>
                <?php 
                if ($fundDetailsArr['private_hospital_admission'][4]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Admission into public hospital as a private patient. Generally overnight accommodation in a private or shared room, same day admissions, emergency department facility fees (if the service leads to an admission or it is continuation of care following an admission)">Public hospital admission</td>
                <?php 
                if ($fundDetailsArr['private_hospital_admission'][4]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Any accidential injury. For accident to be covered, treatment must be sought through a Doctor or an Emergency Department within 48 hours of sustaining the injury.">Accidents & emergency services</td>
                <?php 
                if ($fundDetailsArr['accidents_and_emergency_services'][4]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="When you needed an ambulance to be transported to hospital, health insurance providers will pay 100% cover for when you need an emergency ambulance services.">Emergency Ambulance</td>
                <?php 
                if ($fundDetailsArr['emergency_ambulance'][4]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="If someone on your membership has to return to your home country due to a substantial life-altering illness or injury, health providers may arrange and pay the reasonable cost of return travel with the appropriate medical supervision.">Repatriation to home country</td>
                <?php 
                if ($fundDetailsArr['repartation_to_home_country'][4]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">Excesses</td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="This is the amount you pay towards your hospital treatment before health insurance providers make any benefits. It's paid once per person per calendar year. It does not apply to children on a family policy.">Excess option</td>
                <td class="row-gray"><?php echo $fundDetailsArr['excess_option'][4]; ?></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">Some examples of what services are covered</td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="When you needed an ambulance to be transported to hospital, health insurance providers will pay 100% cover for when you need an emergency ambulance services.">Ambulance services</td>
                <?php 
                if ($fundDetailsArr['ambulance_services'][4]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Any accidential injury. For accident to be covered, treatment must be sought through a Doctor or an Emergency Department within 48 hours of sustaining the injury.">Accidential injury</td>
                <?php 
                if ($fundDetailsArr['accidential_injury'][4]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Services and treatment provided to an admitted patient in hospital that deal with the care of women during pregnancy, childbirth and following delivery. Includes regular and caesarean childbirth and in-patient diagnostic imaging, as well as treatment of pregnancy related complications.">Pregnancy and related services</td>
                <?php 
                if ($fundDetailsArr['pregnancy_and_related_services'][4]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Reconstructions to repair ligament tears, remove loose tissue and to treat other damage. Surgery to replace a hip or knee joint with a prosthesis. Its aim is to relieve pain and/or increase function by replacing all or part of the joint resurface.">Joint replacements surgery (hip, knee, shoulder)</td>
                <?php 
                if ($fundDetailsArr['join_replacements_surgery'][4]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="A procedure to examine the lining of the large bowel (colon). It is used to investigate the cause of abdominal pain, bleeding, irregular bowel habits, to remove polyps or detect cancer.">Colonoscopy</td>
                <?php 
                if ($fundDetailsArr['colonoscopy'][4]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="There are 200 different type of cancers and most area of the body can be affected. Some health insurance providers will cover some or all cancer treatments.">Cancer treatments</td>
                <?php 
                if ($fundDetailsArr['cancer_treatments'][4]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Surgery for cataracts, as well as other eye lens related hospital admissions.">Eye surgery</td>
                <?php 
                if ($fundDetailsArr['eye_surgery'][4]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Medical and surgical admissions intended to investigate, diagnose, monitor and/or treat heart-related conditions. May include services such as open heart and bypass surgery, and invasive cardiac investigations and procedures such as angiograms, angioplasties and stent insertions.">Heart surgery</td>
                <?php 
                if ($fundDetailsArr['heart_surgery'][4]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Admission to hospital for medication and possible removal of an inflamed appendix (appendicitis).">Appendicitis treatment</td>
                <?php 
                if ($fundDetailsArr['appendicitis_treatment'][4]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="A gastroscopy is performed to investigate or diagnose swallowing, stomach pain and ulcers.">Gastroscopies</td>
                <?php 
                if ($fundDetailsArr['gastroscopies'][4]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Treatment aimed at assisting or replacing the function of the kidneys by ensuring the appropriate balance of chemicals in the blood. It can include both haemodialysis (circulating the blood through a machine) and peritoneal dialysis (infusing and draining a sterile solution into the abdomen).">Kidney failure treatment</td>
                <?php 
                if ($fundDetailsArr['kidney_failure_treatment'][4]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Therapy that assists in recovery following a major health event, such as after a joint replacement or following a heart attack. It must be provided at an approved rehabilitation facility and under an approved program.">Rehabilitation treatment</td>
                <?php 
                if ($fundDetailsArr['rehabilitation_treatment'][4]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="In Vitro Fertilisation (IVF) is an assisted reproductive technologies (ART) used to help infertile couple to conceive a child.">Fertility treatment (IVF)</td>
                <?php 
                if ($fundDetailsArr['fertility_treatment'][4]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Surgery to improve your appearance that is not medically necessary.">Cosmetic surgery</td>
                 <?php 
                if ($fundDetailsArr['cosmetic_surgery'][4]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="The Medicare Benefits Schedule (MBS) lists all the medical services subsidised by the Australian government through Medicare. This includes thousands of in-hospital services that we may pay benefits towards.">All other in-hospital services where a Medicare benefit is payable</td>
                <?php 
                  if ($fundDetailsArr['other_hospital_services'][4]=="Yes") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "";
                  }
                  else if ($fundDetailsArr['other_hospital_services'][4]=="Yes | 100% of MBS") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "100% of MBS";
                  }
                  else{
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                    $text = "";
                  } 
                ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ><br><?php echo $text; ?></td>
              </tr>
              <tr>
                <td class="row-gray policy_document">Policy Document</td>
                <td class="row-gray policy_document pdf"><a href="<?php echo $fundDetailsArr['policy_document'][4]; ?>" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
              </tr>
              <tr>
                <td colspan="2" class="row-light-blue"><br><a href="javascript:void(0);" data-url="<?php echo $fundDetailsArr['policy_url'][4] ?>" name="buy_now" class="buy-now-btn">Select Now</a><br><br></td>
              </tr>
            </table>
            <table class="result-table hide">
              <tr>
                <td colspan="2" class="text-center"><img src="<?php echo $fundDetailsArr['policy_image_url'][5]; ?>"></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue fund-name fixed_head"><img class="left-arrow" src="<?php echo get_stylesheet_directory_uri() ?>/images/left_arrow.png"><span class="policy-company"><?php echo $fundDetailsArr['policy_name'][5]; ?></span><img class="right-arrow" src="<?php echo get_stylesheet_directory_uri() ?>/images/right_arrow.png"></td>
              </tr>
              <tr>
                <td colspan="2" class="row-light-gray"><p class="premium">$<?php echo $fundDetailsArr['weekly_premimum_price'][5]; ?></p><p class="premium-type">Weekly</p><a href="javascript:void(0);" data-url="<?php echo $fundDetailsArr['policy_url'][5] ?>" name="buy_now" class="buy-now-btn">Select Now</a><br><br></td>
              </tr>
              <!-- <tr>
                <td colspan="2" class="row-dark-blue row-header">General</td>
              </tr>
              <tr>
                <td class="row-gray">Level of cover</td>
                <td class="row-gray"><?php echo $fundDetailsArr['level_of_cover'][5]; ?></td>
              </tr> -->
              <tr>
                <td class="row-light-blue">About this cover</td>
                <td class="row-light-blue"><?php echo $fundDetailsArr['about_this_cover'][5]; ?></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">Visa Compliance</td>
                </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Does this health insurance cover meeds the condition of the Australian Department of Immigration and Border Protection's visa requirements.">Meets government requirements</td>
                <?php 
                if ($fundDetailsArr['meets_govt_requirements'][5]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>"></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Some insurance will immediately issue a letter confirming your health insurance. You can use this letter as a proof of insurance with your visa application.">Instant visa letter issued</td>
                <?php 
                if ($fundDetailsArr['instant_visa_letter_issued'][5]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>"></td>
              </tr>
              <tr><td class="row-gray">Visa types</td>
                <td class="row-gray"><?php echo $fundDetailsArr['visa_types'][5]; ?></td>
                </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">What's Included</td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Helps towards the hospital accommodation costs and doctors, specialists, surgeons and anaesthetists fees for the hospital services (see some example services below).">Hospital expenses</td>
                <?php 
                if ($fundDetailsArr['hospital_expenses'][5]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="When you’re treated in hospital by a doctor, surgeon or anaesthetist the set fee for their service is called the Medicare Benefits Schedule (MBS) fee. If the person treating you charges only the MBS fee you won’t need to pay anything. If they choose to charge above the MBS fee you will likely have to pay an out-of-pocket cost to cover the difference.">Medical expenses (in hospital)</td>
                <?php 
                  if ($fundDetailsArr['medical_expenses_inhospital'][5]=="Yes") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "";
                  }
                  else if ($fundDetailsArr['medical_expenses_inhospital'][5]=="Yes | 100% of MBS") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "100% of MBS";
                  }
                  else{
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                    $text = "";
                  } 
                ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ><br><?php echo $text; ?></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Admission into private hospital. Generally overnight accommodation in a private or shared room, same day admissions, intensive care, operating theatre fees, etc.">Private hospital admission</td>
                <?php 
                if ($fundDetailsArr['private_hospital_admission'][5]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Admission into public hospital as a private patient. Generally overnight accommodation in a private or shared room, same day admissions, emergency department facility fees (if the service leads to an admission or it is continuation of care following an admission)">Public hospital admission</td>
                <?php 
                if ($fundDetailsArr['private_hospital_admission'][5]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Any accidential injury. For accident to be covered, treatment must be sought through a Doctor or an Emergency Department within 48 hours of sustaining the injury.">Accidents & emergency services</td>
                <?php 
                if ($fundDetailsArr['accidents_and_emergency_services'][5]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="When you needed an ambulance to be transported to hospital, health insurance providers will pay 100% cover for when you need an emergency ambulance services.">Emergency Ambulance</td>
                <?php 
                if ($fundDetailsArr['emergency_ambulance'][5]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="If someone on your membership has to return to your home country due to a substantial life-altering illness or injury, health providers may arrange and pay the reasonable cost of return travel with the appropriate medical supervision.">Repatriation to home country</td>
                <?php 
                if ($fundDetailsArr['repartation_to_home_country'][5]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">Excesses</td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="This is the amount you pay towards your hospital treatment before health insurance providers make any benefits. It's paid once per person per calendar year. It does not apply to children on a family policy.">Excess option</td>
                <td class="row-gray"><?php echo $fundDetailsArr['excess_option'][5]; ?></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">Some examples of what services are covered</td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="When you needed an ambulance to be transported to hospital, health insurance providers will pay 100% cover for when you need an emergency ambulance services.">Ambulance services</td>
                <?php 
                if ($fundDetailsArr['ambulance_services'][5]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Any accidential injury. For accident to be covered, treatment must be sought through a Doctor or an Emergency Department within 48 hours of sustaining the injury.">Accidential injury</td>
                <?php 
                if ($fundDetailsArr['accidential_injury'][5]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Services and treatment provided to an admitted patient in hospital that deal with the care of women during pregnancy, childbirth and following delivery. Includes regular and caesarean childbirth and in-patient diagnostic imaging, as well as treatment of pregnancy related complications.">Pregnancy and related services</td>
                <?php 
                if ($fundDetailsArr['pregnancy_and_related_services'][5]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Reconstructions to repair ligament tears, remove loose tissue and to treat other damage. Surgery to replace a hip or knee joint with a prosthesis. Its aim is to relieve pain and/or increase function by replacing all or part of the joint resurface.">Joint replacements surgery (hip, knee, shoulder)</td>
                <?php 
                if ($fundDetailsArr['join_replacements_surgery'][5]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="A procedure to examine the lining of the large bowel (colon). It is used to investigate the cause of abdominal pain, bleeding, irregular bowel habits, to remove polyps or detect cancer.">Colonoscopy</td>
                <?php 
                if ($fundDetailsArr['colonoscopy'][5]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="There are 200 different type of cancers and most area of the body can be affected. Some health insurance providers will cover some or all cancer treatments.">Cancer treatments</td>
                <?php 
                if ($fundDetailsArr['cancer_treatments'][5]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Surgery for cataracts, as well as other eye lens related hospital admissions.">Eye surgery</td>
                <?php 
                if ($fundDetailsArr['eye_surgery'][5]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Medical and surgical admissions intended to investigate, diagnose, monitor and/or treat heart-related conditions. May include services such as open heart and bypass surgery, and invasive cardiac investigations and procedures such as angiograms, angioplasties and stent insertions.">Heart surgery</td>
                <?php 
                if ($fundDetailsArr['heart_surgery'][5]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Admission to hospital for medication and possible removal of an inflamed appendix (appendicitis).">Appendicitis treatment</td>
                <?php 
                if ($fundDetailsArr['appendicitis_treatment'][5]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="A gastroscopy is performed to investigate or diagnose swallowing, stomach pain and ulcers.">Gastroscopies</td>
                <?php 
                if ($fundDetailsArr['gastroscopies'][5]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Treatment aimed at assisting or replacing the function of the kidneys by ensuring the appropriate balance of chemicals in the blood. It can include both haemodialysis (circulating the blood through a machine) and peritoneal dialysis (infusing and draining a sterile solution into the abdomen).">Kidney failure treatment</td>
                <?php 
                if ($fundDetailsArr['kidney_failure_treatment'][5]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Therapy that assists in recovery following a major health event, such as after a joint replacement or following a heart attack. It must be provided at an approved rehabilitation facility and under an approved program.">Rehabilitation treatment</td>
                <?php 
                if ($fundDetailsArr['rehabilitation_treatment'][5]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="In Vitro Fertilisation (IVF) is an assisted reproductive technologies (ART) used to help infertile couple to conceive a child.">Fertility treatment (IVF)</td>
                <?php 
                if ($fundDetailsArr['fertility_treatment'][5]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Surgery to improve your appearance that is not medically necessary.">Cosmetic surgery</td>
                 <?php 
                if ($fundDetailsArr['cosmetic_surgery'][5]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="The Medicare Benefits Schedule (MBS) lists all the medical services subsidised by the Australian government through Medicare. This includes thousands of in-hospital services that we may pay benefits towards.">All other in-hospital services where a Medicare benefit is payable</td>
                <?php 
                  if ($fundDetailsArr['other_hospital_services'][5]=="Yes") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "";
                  }
                  else if ($fundDetailsArr['other_hospital_services'][5]=="Yes | 100% of MBS") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "100% of MBS";
                  }
                  else{
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                    $text = "";
                  } 
                ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ><br><?php echo $text; ?></td>
              </tr>
              <tr>
                <td class="row-gray policy_document">Policy Document</td>
                <td class="row-gray policy_document pdf"><a href="<?php echo $fundDetailsArr['policy_document'][5]; ?>" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
              </tr>
              <tr>
                <td colspan="2" class="row-light-blue"><br><a href="javascript:void(0);" data-url="<?php echo $fundDetailsArr['policy_url'][5] ?>" name="buy_now" class="buy-now-btn">Select Now</a><br><br></td>
              </tr>
            </table>
            <table class="result-table hide">
              <tr>
                <td colspan="2" class="text-center"><img src="<?php echo $fundDetailsArr['policy_image_url'][6]; ?>"></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue fund-name fixed_head"><img class="left-arrow" src="<?php echo get_stylesheet_directory_uri() ?>/images/left_arrow.png"><span class="policy-company"><?php echo $fundDetailsArr['policy_name'][6]; ?></span></td>
              </tr>
              <tr>
                <td colspan="2" class="row-light-gray"><p class="premium">$<?php echo $fundDetailsArr['weekly_premimum_price'][6]; ?></p><p class="premium-type">Weekly</p><a href="javascript:void(0);" data-url="<?php echo $fundDetailsArr['policy_url'][6] ?>" name="buy_now" class="buy-now-btn">Select Now</a><br><br></td>
              </tr>
              <!-- <tr>
                <td colspan="2" class="row-dark-blue row-header">General</td>
              </tr>
              <tr>
                <td class="row-gray">Level of cover</td>
                <td class="row-gray"><?php echo $fundDetailsArr['level_of_cover'][6]; ?></td>
              </tr> -->
              <tr>
                <td class="row-light-blue">About this cover</td>
                <td class="row-light-blue"><?php echo $fundDetailsArr['about_this_cover'][6]; ?></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">Visa Compliance</td>
                </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Does this health insurance cover meeds the condition of the Australian Department of Immigration and Border Protection's visa requirements.">Meets government requirements</td>
                <?php 
                if ($fundDetailsArr['meets_govt_requirements'][6]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>"></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Some insurance will immediately issue a letter confirming your health insurance. You can use this letter as a proof of insurance with your visa application.">Instant visa letter issued</td>
                <?php 
                if ($fundDetailsArr['instant_visa_letter_issued'][6]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>"></td>
              </tr>
              <tr><td class="row-gray">Visa types</td>
                <td class="row-gray"><?php echo $fundDetailsArr['visa_types'][6]; ?></td>
                </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">What's Included</td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Helps towards the hospital accommodation costs and doctors, specialists, surgeons and anaesthetists fees for the hospital services (see some example services below).">Hospital expenses</td>
                <?php 
                if ($fundDetailsArr['hospital_expenses'][6]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="When you’re treated in hospital by a doctor, surgeon or anaesthetist the set fee for their service is called the Medicare Benefits Schedule (MBS) fee. If the person treating you charges only the MBS fee you won’t need to pay anything. If they choose to charge above the MBS fee you will likely have to pay an out-of-pocket cost to cover the difference.">Medical expenses (in hospital)</td>
                <?php 
                  if ($fundDetailsArr['medical_expenses_inhospital'][6]=="Yes") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "";
                  }
                  else if ($fundDetailsArr['medical_expenses_inhospital'][6]=="Yes | 100% of MBS") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "100% of MBS";
                  }
                  else{
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                    $text = "";
                  } 
                ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ><br><?php echo $text; ?></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Admission into private hospital. Generally overnight accommodation in a private or shared room, same day admissions, intensive care, operating theatre fees, etc.">Private hospital admission</td>
                <?php 
                if ($fundDetailsArr['private_hospital_admission'][6]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Admission into public hospital as a private patient. Generally overnight accommodation in a private or shared room, same day admissions, emergency department facility fees (if the service leads to an admission or it is continuation of care following an admission)">Public hospital admission</td>
                <?php 
                if ($fundDetailsArr['private_hospital_admission'][6]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Any accidential injury. For accident to be covered, treatment must be sought through a Doctor or an Emergency Department within 48 hours of sustaining the injury.">Accidents & emergency services</td>
                <?php 
                if ($fundDetailsArr['accidents_and_emergency_services'][6]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="When you needed an ambulance to be transported to hospital, health insurance providers will pay 100% cover for when you need an emergency ambulance services.">Emergency Ambulance</td>
                <?php 
                if ($fundDetailsArr['emergency_ambulance'][6]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="If someone on your membership has to return to your home country due to a substantial life-altering illness or injury, health providers may arrange and pay the reasonable cost of return travel with the appropriate medical supervision.">Repatriation to home country</td>
                <?php 
                if ($fundDetailsArr['repartation_to_home_country'][6]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">Excesses</td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="This is the amount you pay towards your hospital treatment before health insurance providers make any benefits. It's paid once per person per calendar year. It does not apply to children on a family policy.">Excess option</td>
                <td class="row-gray"><?php echo $fundDetailsArr['excess_option'][6]; ?></td>
              </tr>
              <tr>
                <td colspan="2" class="row-dark-blue row-header">Some examples of what services are covered</td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="When you needed an ambulance to be transported to hospital, health insurance providers will pay 100% cover for when you need an emergency ambulance services.">Ambulance services</td>
                <?php 
                if ($fundDetailsArr['ambulance_services'][6]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Any accidential injury. For accident to be covered, treatment must be sought through a Doctor or an Emergency Department within 48 hours of sustaining the injury.">Accidential injury</td>
                <?php 
                if ($fundDetailsArr['accidential_injury'][6]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Services and treatment provided to an admitted patient in hospital that deal with the care of women during pregnancy, childbirth and following delivery. Includes regular and caesarean childbirth and in-patient diagnostic imaging, as well as treatment of pregnancy related complications.">Pregnancy and related services</td>
                <?php 
                if ($fundDetailsArr['pregnancy_and_related_services'][6]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Reconstructions to repair ligament tears, remove loose tissue and to treat other damage. Surgery to replace a hip or knee joint with a prosthesis. Its aim is to relieve pain and/or increase function by replacing all or part of the joint resurface.">Joint replacements surgery (hip, knee, shoulder)</td>
                <?php 
                if ($fundDetailsArr['join_replacements_surgery'][6]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="A procedure to examine the lining of the large bowel (colon). It is used to investigate the cause of abdominal pain, bleeding, irregular bowel habits, to remove polyps or detect cancer.">Colonoscopy</td>
                <?php 
                if ($fundDetailsArr['colonoscopy'][6]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="There are 200 different type of cancers and most area of the body can be affected. Some health insurance providers will cover some or all cancer treatments.">Cancer treatments</td>
                <?php 
                if ($fundDetailsArr['cancer_treatments'][6]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Surgery for cataracts, as well as other eye lens related hospital admissions.">Eye surgery</td>
                <?php 
                if ($fundDetailsArr['eye_surgery'][6]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Medical and surgical admissions intended to investigate, diagnose, monitor and/or treat heart-related conditions. May include services such as open heart and bypass surgery, and invasive cardiac investigations and procedures such as angiograms, angioplasties and stent insertions.">Heart surgery</td>
                <?php 
                if ($fundDetailsArr['heart_surgery'][6]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Admission to hospital for medication and possible removal of an inflamed appendix (appendicitis).">Appendicitis treatment</td>
                <?php 
                if ($fundDetailsArr['appendicitis_treatment'][6]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="A gastroscopy is performed to investigate or diagnose swallowing, stomach pain and ulcers.">Gastroscopies</td>
                <?php 
                if ($fundDetailsArr['gastroscopies'][6]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="Treatment aimed at assisting or replacing the function of the kidneys by ensuring the appropriate balance of chemicals in the blood. It can include both haemodialysis (circulating the blood through a machine) and peritoneal dialysis (infusing and draining a sterile solution into the abdomen).">Kidney failure treatment</td>
                <?php 
                if ($fundDetailsArr['kidney_failure_treatment'][6]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Therapy that assists in recovery following a major health event, such as after a joint replacement or following a heart attack. It must be provided at an approved rehabilitation facility and under an approved program.">Rehabilitation treatment</td>
                <?php 
                if ($fundDetailsArr['rehabilitation_treatment'][6]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="In Vitro Fertilisation (IVF) is an assisted reproductive technologies (ART) used to help infertile couple to conceive a child.">Fertility treatment (IVF)</td>
                <?php 
                if ($fundDetailsArr['fertility_treatment'][6]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-gray tooltipstered" title="Surgery to improve your appearance that is not medically necessary.">Cosmetic surgery</td>
                 <?php 
                if ($fundDetailsArr['cosmetic_surgery'][6]=="Yes") 
                  $src=get_stylesheet_directory_uri()."/images/tick_cover.png";
                else
                  $src=get_stylesheet_directory_uri()."/images/tick_uncover.png"; ?>
                <td class="row-gray"><img src="<?php echo $src; ?>" ></td>
              </tr>
              <tr>
                <td class="row-light-blue tooltipstered" title="The Medicare Benefits Schedule (MBS) lists all the medical services subsidised by the Australian government through Medicare. This includes thousands of in-hospital services that we may pay benefits towards.">All other in-hospital services where a Medicare benefit is payable</td>
                <?php 
                  if ($fundDetailsArr['other_hospital_services'][6]=="Yes") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "";
                  }
                  else if ($fundDetailsArr['other_hospital_services'][6]=="Yes | 100% of MBS") {
                    $src = get_stylesheet_directory_uri()."/images/tick_cover.png";
                    $text = "100% of MBS";
                  }
                  else{
                    $src = get_stylesheet_directory_uri()."/images/tick_uncover.png";
                    $text = "";
                  } 
                ?>
                <td class="row-light-blue"><img src="<?php echo $src; ?>" ><br><?php echo $text; ?></td>
              </tr>
              <tr>
                <td class="row-gray policy_document">Policy Document</td>
                <td class="row-gray policy_document pdf"><a href="<?php echo $fundDetailsArr['policy_document'][6]; ?>" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
              </tr>
              <tr>
                <td colspan="2" class="row-light-blue"><br><a href="javascript:void(0);" data-url="<?php echo $fundDetailsArr['policy_url'][6] ?>" name="buy_now" class="buy-now-btn">Select Now</a><br><br></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </form>
  </div>

<?php // get_footer(); 
include("footer.php"); ?>

<script type="text/javascript">
  (function($){

    $(window).on('load', function(){
      $(".spinner-container").fadeOut("slow");
    });

    $(document).ready(function() {

      $('.previous-btn').hide();

      $('.tooltipstered').tooltipster({
        animation: 'swing',
        maxWidth: 450,
      });

      $( ".right-arrow" ).on( "click", function() {
        var clicked_element = $(this).closest('.result-table');
        $(clicked_element).addClass('hide');
        $(clicked_element).next('.result-table').removeClass('hide');
      });

      $( ".left-arrow" ).on( "click", function() {
        var clicked_element = $(this).closest('.result-table');
        $(clicked_element).addClass('hide');
        $(clicked_element).prev('.result-table').removeClass('hide');
      });

      $( "#cover_type" ).on( "change", function() {
        location.href = $(this).val();
      });

      $( ".nav-btn" ).on( "click", function() {
        var nav_btn_name = $(this).attr('name');
        var displayed_column = $('#displayed_column').val();
        var displayed_columnArr=[];var result = [];var resultArr = [];

        if (nav_btn_name=='next-btn') {
          displayed_columnArr = displayed_column.split(',');
          for (var i = 0; i <= displayed_columnArr.length-1; i++) {
            if (displayed_columnArr.length-1==i) {
              result+= parseInt(displayed_columnArr[i])+parseInt(1);
              resultArr+= parseInt(displayed_columnArr[i])+parseInt(1);
            }
            else {
              result+= parseInt(displayed_columnArr[i])+parseInt(1)+" ,.";
              resultArr+= parseInt(displayed_columnArr[i])+parseInt(1)+",";
            }
          }
          result = "."+result;
          $(result).removeClass('hide');
          $(".results-column:not("+result+")").addClass('hide');
          $('#displayed_column').val(resultArr);
          $('.previous-btn').show();
          if (displayed_column[displayed_column.length-1]==6) {
            $('.next-btn').hide();
          }
        } else {
          var displayed_columnArr = displayed_column.split(',');
          for (var i = 0; i <= displayed_columnArr.length-1; i++) {
            if (displayed_columnArr.length-1==i) {
              result+= parseInt(displayed_columnArr[i])-parseInt(1);
              resultArr+= parseInt(displayed_columnArr[i])-parseInt(1);
            }
            else {
              result+= parseInt(displayed_columnArr[i])-parseInt(1)+" ,.";
              resultArr+= parseInt(displayed_columnArr[i])-parseInt(1)+",";
            }
          }
          result = "."+result;
          $(result).removeClass('hide');
          $(".results-column:not("+result+")").addClass('hide');
          $('#displayed_column').val(resultArr);
          $('.next-btn').show();
          if (displayed_column[0]==2) {
            $('.previous-btn').hide();
          }
        }
      });

      var $cache = $('.fixed-head');
      if ($cache.length) {
        var vTop = $cache.offset().top - parseFloat($cache.css('margin-top').replace(/auto/, 150));
      }

      $(window).scroll(function (event) {
        var y = $(this).scrollTop();
        var scroll = $(window).scrollTop();
        if (y > vTop) {
          if (scroll>=150) {
            $cache.addClass('stuck');
            var header_width = $('.result-table').width();
            $(".stuck").css({"width": header_width+"%", "left": "0"});
          }
        } 
        else {
          $cache.removeClass('stuck');
        }
      });

      $(".buy-now-btn").click(function(){
        var url = $(this).attr("data-url");   
        trackOutboundLink(''+url+'');
        var win = window.open(url, '_blank');
          win.focus();    
        return false;
      });

    });
  })(jQuery);
</script>