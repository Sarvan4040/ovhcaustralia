<?php
/*
Template Name: Insurance Home
*/ 
get_header();
$banner = get_the_post_thumbnail_url();
// check if the post has a Post Thumbnail assigned to it.
$banner_url="";
if ($banner!="")  
    $banner_url = get_the_post_thumbnail_url();
?>

<div class="container-fluid visa-banner-fluid" style="background: url(<?php echo $banner_url ?>) center center no-repeat;background-size: cover;">
    <div class="container visa-banner-container">
        <div class="visa-banner-content">
            <div class="visa-banner-title">
                <h2><span>Overseas Student</span> Health Cover (OSHC)</h2>
                <p>For visa subclass 500 and more.</p>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 visa-overseas-cover">
                <p>Planning to study in Australia or to continue your studies? Medibank’s Overseas Student Health Cover (OSHC) can meet your visa health insurance requirements and help with medical treatment if you have an accident or get sick.</p>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid medibank-content-fluid">
    <div class="container medibank-content-container">
        <h3>Benifits of overseas health cover with Medibank</h3>
        <div class="text-center beat-images">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/beat.png">
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 benifits-overseas">
            <div class="col-md-4 col-sm-4 col-xs-12 benifits-content">
                <div class="col-md-2 col-sm-3 col-xs-2 benifits-image">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/tick-1.png">
                </div>
                <div class="col-md-10 col-sm-9 col-xs-10 benifits-cover">
                    <h4>Visa compliant cover</h4>
                    <p>A range of cover options that can meat. Your visa health insurance requirements for working studying in Australia.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 benifits-content">
                <div class="col-md-2 col-sm-3 col-xs-2">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/add-1.png">
                </div>
                <div class="col-md-10 col-sm-9 col-xs-10 benifits-cover">
                    <h4>Product against the unexpected</h4>
                    <p>Unlimited emergency ambulance trips to hospital</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 benifits-content">
                <div class="col-md-2 col-sm-3 col-xs-2 benifits-image">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/star-1.png">
                </div>
                <div class="col-md-10 col-sm-9 col-xs-10 benifits-cover">
                    <h4>Trusted partner</h4>
                    <p>3.8 million members in Astralia.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid looking-fluid">
    <div class="container looking-container">
        <h3>I am looking for particular cover</h3>
        <div class="text-center beat-images">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/beat.png">
        </div>
        <p>View full range of cover for:</p>
        <div class="col-md-12 col-sm-12 col-xs-12 looking-particular">
            <div class="cover-button">
                <div class="btn btn-cover">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/single.png" class="static-image">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/single-1.png" class="hover-image">
                    <a href="javascript:void(0);"> Singles cover</a>
                    <i class="fa fa-angle-right right-arrow"></i>
                </div>
                <div class="btn btn-cover">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/couple.png" class="static-image">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/couple-1.png" class="hover-image">
                    <a href="javascript:void(0);">Couples cover</a>
                    <i class="fa fa-angle-right right-arrow"></i>
                </div>
                <div class="btn btn-cover">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/family.png" class="static-image">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/family-1.png" class="hover-image">
                    <a href="javascript:void(0);">Families cover</a> 
                    <i class="fa fa-angle-right right-arrow"></i></div>
                <div class="btn btn-cover">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/single-parent.png" class="static-image">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/single-parent-1.png" class="hover-image">
                    <a href="javascript:void(0);">Single Parents cover</a>
                    <i class="fa fa-angle-right right-arrow"></i>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>