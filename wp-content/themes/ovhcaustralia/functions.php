<?php

add_theme_support( 'post-thumbnails' );

register_nav_menus( array(
    'primary' => __( 'Primary Menu', '' ),
) );

function enqueue_scripts() {
    // Add custom fonts, used in the main stylesheet.
    wp_enqueue_style( 'bootstrap', get_theme_file_uri( '/assets/css/bootstrap.min.css' ), '', null );
    // Theme stylesheet.
    wp_enqueue_style( 'main', get_theme_file_uri( '/assets/css/main.css' ), '', null  );
    wp_enqueue_style( 'style', get_theme_file_uri( '/style.css' ));
    wp_enqueue_style( 'tooltipster', get_theme_file_uri( '/assets/css/tooltipster.css' ));

    wp_enqueue_script( 'jquery', get_theme_file_uri( '/assets/js/jquery.min.js' ),  array('jquery'), null, true );
    wp_enqueue_script( 'bootstrap', get_theme_file_uri( '/assets/js/bootstrap.min.js' ),  array('jquery'), null, true );
    wp_enqueue_script( 'tooltipster', get_theme_file_uri( '/assets/js/tooltipster.min.js' ),  array('jquery'), null, true );
}
add_action( 'wp_enqueue_scripts', 'enqueue_scripts' );