
<footer>
	<div class="container-fluid footer-fluid">
		<div class="container footer-container">
			<div class="col-md-12 col-sm-12 col-xs-12 footer-main">
				<div class="col-md-3 col-sm-3 col-xs-12 footer-menu">
					<h4>OVHC Australia</h4>
					<ul>
						<li><a href="javascript:viod(0);">About</a></li>
						<li><a href="javascript:viod(0);">Contact us</a></li>
					</ul>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12 footer-menu">
					<h4>Assistance</h4>
					<ul>
						<li><a href="javascript:viod(0);">Working in Australia</a></li>
						<li><a href="javascript:viod(0);">Studying in Australia</a></li>
						<li><a href="javascript:viod(0);">Visiting Australia</a></li>
						<li><a href="javascript:viod(0);">Goverment Info</a></li>
						<li><a href="javascript:viod(0);">Support</a></li>
					</ul>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12 footer-menu">
					<h4>Legals</h4>
					<ul>
						<li><a href="javascript:viod(0);">Privacy Policy</a></li>
						<li><a href="javascript:viod(0);">Terms and Conditions</a></li>
					</ul>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12 footer-menu">
					<h4 class="social-connect">Connect with us</h4>
					<ul class="social-menu">
						<li class="social-media-icons">
		                    <a href="javascript:viod(0);" title="Facebook" id="ai-Medibank"><i class="fa fa-facebook-square"></i></a>
		                    <a href="javascript:viod(0);" title="Twitter" id="ai-medibank"><i class="fa fa-twitter-square"></i></a>
		                </li>
						<li><a href="javascript:viod(0);">Find a store</a></li>
						<li><a href="javascript:viod(0);">Contact us</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-content">
		<p>(c) 2018 OVHC Australia | All rights reserved</p>
	</div>
</footer>
</body>
</html>
<?php wp_footer(); ?>